<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasklist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul_tasklist', 255);
            $table->string('keterangan_tasklist', 255);
            $table->dateTime('tanggal_mulai');
            $table->dateTime('tanggal_target');
            $table->dateTime('tanggal_selesai');
            $table->bigInteger('project_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasklist');
    }
}
