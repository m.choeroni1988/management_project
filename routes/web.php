<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contoh', function(){
    return "Test";
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/client', function() {
    return view('client.form_client');
});


Route::prefix('client')->group(
    function() {
        Route::get('/', function() {
            return view('client.form_client');
        });
        
        Route::post('create', 'ClientController@create');
    }
);
