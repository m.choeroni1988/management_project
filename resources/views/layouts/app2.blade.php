<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>CRUD Client</title>
</head>
<body>
  @yield('content')
</body>
</html>