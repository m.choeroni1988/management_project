<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/fontawesome_5.12.0/css/all.css') }}" rel="stylesheet">
    <style>
      .sidenav {
        height: 100%;
        width: 0px;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-image: linear-gradient(0deg,#7e8ea1,#3c4655);
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 78px;
      }

      #app .sidebar {
        width: 13.75rem;
        border-right: none;
        background-color: transparent;
        background-image: linear-gradient(0deg,#7e8ea1,#3c4655);
      }

      .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 16px;
        color: #818181;
        display: block;
        transition: 0.3s;
      }

      .sidenav a:hover {
        color: #f1f1f1;
      }

      .sidenav .closebtn {
        position: absolute;
        top: 50px;
        right: 0px;
        font-size: 30px;
        margin-left: 50px;
      }

      @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
      }

      @media (min-width: 1200px) {
        .container-xl, .container-lg, .container-md, .container-sm, .container {
            max-width: 100%;
        }
      }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" style="position: absolute; width: 100%; z-index: 999;">
            <div class="container">
              <span style="font-size:26px;cursor:pointer" onclick="openCloseNav()">&#9776;</span>
                <a class="navbar-brand" href="{{ url('/') }}" style="font-size: 26px; padding-left: 10px;">
                    {{ config('app.name', ' Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
          <div class="container" style="padding-top: 60px;">
            <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onclick="openCloseNav()">&times;</a>
              <a href="#">
                <span class="fas fa-tachometer-alt" style="margin-right: 10px;"></span>
                Dashboard
              </a>
              <a href="{{ url('client') }}">
                <span class="fas fa-address-card" style="margin-right: 10px;"></span>
                Client
              </a>
              <a href="#">
                <span class="fas fa-home" style="margin-right: 10px;"></span>
                Clients
              </a>
              <a href="#">
                <span class="fas fa-home" style="margin-right: 10px;"></span>
                Contact
              </a>
            </div>
            @yield('content')
          </div>
        </main>
    </div>

    <script>
      function openCloseNav() {
        if (document.getElementById("mySidenav").style.width!="250px") {
          document.getElementById("mySidenav").style.width = "250px";
        } else {
          document.getElementById("mySidenav").style.width = "0px";
        }
      }
      </script>
</body>
</html>
