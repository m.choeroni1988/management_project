@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Client</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('client/create') }}">
                        @csrf

                        <div class="form-group row">
                          <label for="name" class="col-md-4 col-form-label text-md-right">Nama Client</label>
                          <div class="col-md-6">
                            <input id="nama_client" type="text" class="form-control" name="nama_client" value="" placeholder="Nama Client">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="name" class="col-md-4 col-form-label text-md-right">Alamat</label>
                          <div class="col-md-6">
                            <input id="alamat_client" type="text" class="form-control" name="alamat_client" value="" placeholder="Alamat">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="name" class="col-md-4 col-form-label text-md-right">Telepon</label>
                          <div class="col-md-6">
                            <input id="telepon_client" type="text" class="form-control" name="telepon_client" value="" placeholder="Telepon">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="name" class="col-md-4 col-form-label text-md-right">Email</label>
                          <div class="col-md-6">
                            <input id="email_client" type="text" class="form-control" name="email_client" value="" placeholder="Email">
                          </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                  Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
