<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table    = 'client';

    protected $fillable = [
        'id',
        'nama_client',
        'alamat_client',
        'telepon_client',
        'email_client'
    ];

    protected $hidden = [
        
    ];
}
